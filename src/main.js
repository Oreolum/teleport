import { createApp } from 'vue'
import { Quasar } from 'quasar'
import quasarLang from 'quasar/lang/ru'
import { createStore } from 'vuex'
import store from './store/index.js'

// Import icon libraries
import '@quasar/extras/material-icons/material-icons.css'

// Import Quasar css
import 'quasar/src/css/index.sass'
import App from './App.vue'

import './assets/styles/main.scss'

const appStore = createStore(store)

const myApp = createApp(App)
myApp.use(Quasar, {
  plugins: {}, // import Quasar plugins and add here
  lang: quasarLang,
})

myApp.use(appStore)

myApp.mount('#app')
