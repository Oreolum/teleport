export default {
  state() {
    return {
      auth: false,
      users: [
        {
          id: 1,
          phone: '+7 (995) 123-45-67',
          fio: 'Иванов Иван Иванович',
          balance: 300,
          status: true,
        },
        {
          id: 2,
          phone: '+7 (995) 987-45-67',
          fio: 'Петров Петр Петрович',
          balance: 400,
          status: false,
        },
        {
          id: 3,
          phone: '+7 (995) 263-45-67',
          fio: 'Александров Александр Александрович',
          balance: 100,
          status: true,
        },
        {
          id: 4,
          phone: '+7 (995) 715-45-67',
          fio: 'Романов Роман Романович',
          balance: 200,
          status: false,
        },
        {
          id: 5,
          phone: '+7 (995) 931-45-67',
          fio: 'Анатольев Анатолий Анатольевич',
          balance: 500,
          status: true,
        },
        {
          id: 6,
          phone: '+7 (995) 515-45-67',
          fio: 'Константинов Константин Константинович',
          balance: 700,
          status: true,
        },
      ],
      newUser: {
        phone: '',
        fio: '',
        balance: '',
        status: true,
      },
      currentUser: {},
      modalUserForm: false,
      modalUserAction: '',
    }
  },
  mutations: {
    setAuth(state, payload) {
      state.auth = payload
    },
    setCurrentUser(state, payload) {
      state.currentUser = payload
    },
    setModalUserForm(state, payload) {
      state.modalUserForm = payload
    },
    setModalUserAction(state, payload) {
      state.modalUserAction = payload
    },
    userAdd(state, payload) {
      state.users.push(payload)
    },
    userEdit(state, payload) {
      const index = state.users.findIndex((item) => item.id === payload.id)
      state.users[index] = payload
    },
    setUserStatus(state, payload) {
      const index = state.users.findIndex((item) => item.id === payload.userId)
      state.users[index].status = payload.value
    },
  },
  actions: {
    login({ commit }, payload) {
      if (payload.login === 'test' && payload.password === 'test') {
        commit('setAuth', true)
      }
    },
    addUser({ state, commit }) {
      const user = state.newUser
      user.id = state.users.length + 1
      commit('setCurrentUser', Object.assign({}, user))
      commit('setModalUserAction', 'add')
      commit('setModalUserForm', true)
    },
    editUser({ state, commit }, payload) {
      const user = state.users.find((item) => item.id === payload.userId)
      commit('setCurrentUser', Object.assign({}, user))
      commit('setModalUserAction', 'edit')
      commit('setModalUserForm', true)
    },
    saveUser({ commit }, payload) {
      if (payload.action === 'add') {
        commit('userAdd', payload.user)
      }
      if (payload.action === 'edit') {
        commit('userEdit', payload.user)
      }
      commit('setCurrentUser', {})
      commit('setModalUserForm', false)
    },
    changeUserStatus({ commit }, payload) {
      commit('setUserStatus', payload)
    },
  },
}
